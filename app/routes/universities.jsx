import { useState, useEffect } from 'react'
import { json } from "@remix-run/node";
import { Outlet, useLoaderData } from "@remix-run/react"
import axios from "axios";
import { useNavigate } from "react-router-dom";

const fetchData = async () => {
  const response = await axios.get('http://universities.hipolabs.com/search')
    .then(res => res)
    .catch(err => console.log(err))

  return response.data;
}

export async function loader() {
  const universities = await fetchData();
  return json(await universities);
}

const Index = () => {
  const [countries, setCountries] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState("")
  const data = useLoaderData();
  let navigate = useNavigate();

  useEffect(() => {
    if (data?.length) {
      let tempArr = [];
      data.forEach(item => {
        tempArr.push(item.country)
      })

      setSelectedCountry([... new Set(tempArr)][0])
      setCountries([... new Set(tempArr)]);
    }
  }, [])


  return (
    <>
      <span>Total Universities Found : {data?.length}</span>
      <br />
      <span>Select Country to see list of Universities</span>
      <br />
      <select value={selectedCountry} onChange={e => setSelectedCountry(e.target.value)}>
        {countries?.map((item, index) => <option value={item} key={index}>{item}</option>)}
      </select>
      <br/>
      <button onClick={() => navigate(`${selectedCountry}`)}>Search</button>
      <main>
        <Outlet />
      </main>
    </>
  )
}

export default Index;