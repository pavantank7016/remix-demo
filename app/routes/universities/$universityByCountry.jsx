import { useLoaderData } from "@remix-run/react"
import axios from "axios";

const fetchUniversityByCountry = async (country) => {
  const response = await axios.get(`http://universities.hipolabs.com/search?country=${country}`)
    .then(res => res)
    .catch(err => console.log(err))

  return response.data;
}

export async function loader({params}) {
  const universities = await fetchUniversityByCountry(params.universityByCountry);
  return {
    country: params.universityByCountry,
    list: universities
  };
}

const UniversityById = () => {
  const data = useLoaderData();
  console.log('data', data)

  return (
    <div>
      <h3>Total {data.list?.length} Universities Found in {data.country}</h3>
      {data.list?.map((item, index) => {
        return <span style={{
          display: 'block',
          paddingBlock: '5px'
        }} key={index}>{index + 1}. {item.name} - {item.country}</span>
      })}
    </div>
  )
}

export default UniversityById;